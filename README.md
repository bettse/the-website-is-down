# the-website-is-down

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

[![Netlify Status](https://api.netlify.com/api/v1/badges/ae4757a5-0af4-4916-adc5-09af9adb3ddf/deploy-status)](https://app.netlify.com/sites/the-website-is-down/deploys)

## Available Scripts

In the project directory, you can run:

### `netlify dev`

Runs the app in the development mode.\
Open [http://localhost:9000](http://localhost:9000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

