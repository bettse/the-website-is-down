import React, {useEffect} from 'react';

import { If, Then, Else, When } from 'react-if';

import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

import useLocalStorage from './useLocalStorage';
import Search from './Search';

function App() {
  const [apiKey, setApiKey] = useLocalStorage('apiKey', null);

  useEffect(() => {
    const { location } = window;
    const { search } = location;
    const searchParams = new URLSearchParams(search);
    const apiKey = searchParams.get('apiKey');
    if (apiKey) {
      setApiKey(apiKey);
      window.location = window.location.pathname;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    const { target } = event;
    const data = new FormData(target);
    setApiKey(data.get('apiKey'))
  }

  return (
    <>
      <Navbar>
        <Navbar.Brand><a href="/">The Website Is Down</a></Navbar.Brand>
        <Nav>
        <When condition={apiKey !== null}>
          <Nav.Link href="/">{apiKey}</Nav.Link>
        </When>
        </Nav>
      </Navbar>
      <Container>
        <Row>
          <Col>
            <If condition={apiKey == null}>
              <Then>
                <Card>
                  <Card.Body>
                    <Card.Title>NZBgeek API Key</Card.Title>
                    <Form onSubmit={handleSubmit} inline>
                      <Form.Control name="apiKey" placeholder="" />
                      <Button type="submit">Submit</Button>
                    </Form>
                  </Card.Body>
                </Card>
              </Then>
              <Else>
                <Search />
              </Else>
            </If>
          </Col>
        </Row>
      </Container>
      <footer className="page-footer font-small pt-5 mt-5 mx-auto">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
            <small className="text-muted">
            </small>
            </Col>
            <Col>
            <small className="text-muted">
            </small>
            </Col>
            <Col>
            <small className="text-muted">
            </small>
            </Col>
            <Col>
            <small className="text-muted">
            </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default App;
