import React, {useEffect, useState} from 'react';

import { When } from 'react-if';

import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import ListGroup from 'react-bootstrap/ListGroup'
import Pagination from 'react-bootstrap/Pagination'

import RSSParser from 'rss-parser';

import useLocalStorage from './useLocalStorage';

const parser = new RSSParser({
  customFields: {
    feed: ['newznab:response', 'newznab'],
    item: ['newznab:attr', 'newznab', {keepArray: true}],
  }
});
const limit = 20;

function Search () {
  const [apiKey] = useLocalStorage('apiKey', null);
  const [searchTerm, setSearchTerm] = useState(null);
  const [results, setResults] = useState([]);
  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);

  useEffect(() => {
    if (searchTerm) {
      async function fetchData(searchTerm) {
        try {
          const searchParams = new URLSearchParams();
          searchParams.set('apikey', apiKey)
          searchParams.set('t', 'search')
          searchParams.set('extended', 1)
          searchParams.set('password', 0)
          searchParams.set('limit', limit)
          searchParams.set('offset', limit * (page - 1))
          searchParams.set('q', searchTerm)
          //searchParams.set('cat', category)
          const results = await parser.parseURL(`/api?${searchParams.toString()}`);
          setTotal(results['newznab:response']['$']['total'])
          setResults(results.items);
        } catch (err) {
          console.log('parser exception', err);
        }
      }
      fetchData(searchTerm)
    }
  }, [apiKey, page, searchTerm]);

  const handleSubmit = (event) => {
    event.preventDefault();
    const { target } = event;
    const data = new FormData(target);
    setSearchTerm(data.get('searchTerm'))
  }

  const pages = (new Array(total / limit)).fill(0);

  return(
    <>
      <Card>
        <Card.Body>
          <Form onSubmit={handleSubmit} inline>
            <Form.Control name="searchTerm" placeholder="game of thrones" />
            <Button type="submit">Search</Button>
          </Form>
        </Card.Body>
      </Card>
      <When condition={results.length > 0}>
        <ListGroup>
          {results.map((item, i) => {
            const { title, categories, pubDate, link } = item;
            const [category] = categories
            return (
              <ListGroup.Item key={i} action href={link}>
                <p className='m-sm-0 p-sm-0'>
                  <span>[{category}]</span>
                  <span>{title}</span>
                <small>
                  <span className='float-right'>{pubDate}</span>
                </small></p>
              </ListGroup.Item>
              )
          })}
        </ListGroup>
        <Pagination>
          {pages.map((_, i) => {
            return (
              <Pagination.Item key={i} active={i+1 === page} onClick={() => setPage(i+1)}>
                {i+1}
              </Pagination.Item>
            );
          })}
        </Pagination>
      </When>
    </>
  );
}


export default Search;
